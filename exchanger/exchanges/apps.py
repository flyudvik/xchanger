from django.apps import AppConfig


class ExchangesConfig(AppConfig):
    name = 'exchanger.exchanges'
