from django.conf import settings
from django_hosts import host

host_patterns = [
    host(r'www', settings.ROOT_URLCONF, name='www'),
    host(r'office', 'exchanger.offices.urls', name='office'),
]
