from django.contrib import admin

from exchanger.offices.models import Company, ExchangePoint, ExchangeCurrency


class ExchangePointInline(admin.TabularInline):
    model = ExchangePoint


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'owner']
    inlines = [ExchangePointInline]


class ExchangeCurrencyInline(admin.TabularInline):
    model = ExchangeCurrency


@admin.register(ExchangePoint)
class ExchangePointAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'company', 'manager']
    inlines = [ExchangeCurrencyInline]
