import moneyed
from django.contrib.auth import get_user_model
from django.db import models
from model_utils.models import TimeStampedModel

User = get_user_model()


class Company(TimeStampedModel):
    title = models.CharField(max_length=64)
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='companies', null=True, blank=True)


class ExchangePoint(TimeStampedModel):
    company = models.ForeignKey('offices.Company', on_delete=models.SET_NULL, related_name='branches', null=True, blank=True)
    manager = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='branches', null=True, blank=True)

    title = models.CharField(max_length=64)
    address = models.CharField(max_length=255, default='')

    # TODO: add geo location


class ExchangeCurrency(TimeStampedModel):
    branch = models.ForeignKey('offices.ExchangePoint', on_delete=models.CASCADE, related_name='currencies')
    currency = models.CharField(max_length=5, choices=[(code, currency.name) for code, currency in moneyed.CURRENCIES.items()])
